import React from "react";
import { AppBar, Toolbar, Typography } from "@material-ui/core";

//  Styles
import { makeStyles } from "@material-ui/core/styles";
import SideDrawer from "./SideDrawer";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  menuButton: {
    marginRight: theme.spacing(2)
  },
  title: {
    flexGrow: 1
  }
}));

const NavBar = () => {
  const classes = useStyles();
  return (
    <div>
      <AppBar position="static">
        <Toolbar>
          <SideDrawer></SideDrawer>
          <Typography variant="h6" className={classes.title}>
            Application navigation bar
          </Typography>
        </Toolbar>
      </AppBar>
    </div>
  );
};
export default NavBar;
