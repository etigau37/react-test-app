import React, { Component } from "react";
import NavBar from "./navBar";
import tiger from "../resources/cute-tiger.jpg";

class MainFrame extends Component {
  state = {};
  render() {
    return (
      <>
        <NavBar></NavBar>
        <img src={tiger} className="App-logo" alt="logo" />
      </>
    );
  }
}

export default MainFrame;
