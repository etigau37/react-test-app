import React from "react";
import MainFrame from "./components/MainFrame";
//  Styles
import "./App.css";

function App() {
  return (
    <div className="App">
      <MainFrame></MainFrame>
    </div>
  );
}

export default App;
